[General Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_general.md)

[Any% Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_any.md)

Player   |  Time      |  Game Version  |  Platform  |  Date        |  Video Link
:-------:|:----------:|:--------------:|:----------:|:------------:|:--------------------------------------:
Daren_K  |  01:04:17  |  T-6010G       |  Saturn    |  2020-06-06  |  https://www.twitch.tv/videos/644041684
