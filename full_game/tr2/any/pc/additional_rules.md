Tomb Raider II - Additional Rules
=================================

## Game Versions
A list of confirmed game versions can be found [[here]](https://gitlab.com/tombraiderspeedrunning/trs_version_database/tree/master/tr2/pc).

## Whitelisted Tools
* Windowers: [[DxWnd]](https://sourceforge.net/projects/dxwnd/files/), [[D3DWindower]](http://community.pcgamingwiki.com/files/file/733-d3dwindower-english/)
* [[Wine(Linux)]](https://www.winehq.org/)

## Technical Restrictions
* With Windows releases after XP (Vista, 7, 8, 10), the game hangs for about 1s whenever music tracks are read from the CD. A workaround for these operating systems is is to mute the music by lowering the music volume to 0.
