TRS General Submission Rules
==============================

The following are some basic general rules for submitting a run to the TRS records. More specific rules exist for each game/category/platform.
Runs that violate the rules will not be accepted.

## Timing
* In-Game Time (IGT) is used.
* Single-Segment.

## Recording
* Videos with no game audio will not be accepted.
* If you stream your run with music in the background on Twitch/YouTube, your run may get partially muted, so make sure to do a local recording as well. You may have to upload the local recording somewhere for the run to be verified.

## No Cheating
* No cheat codes.
* No modifying game files or overriding hardcoded game values at runtime. (Widescreen hack, deleting game files, No-CD patch, etc.)
* No macro/turbo.
* No reading of game memory values except for IGT related values (autosplitter).

## Technical Restrictions
* Only confirmed game versions listed in the [[TRS Version Database]](https://gitlab.com/tombraiderspeedrunning/trs_version_database) are allowed. If your game version is missing in that database, ask for it to be added provided you have a legitimate copy of the game.
* If a game needs a CD to run, you can create a disc image of your game and mount it.
* Only whitelisted compatibility fixes that are listed in the rules for each game/platform are permitted.
* Console runs will have to be run on a real console.
