[General Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_general.md)

[Any% Rules](https://gitlab.com/tombraiderspeedrunning/trs_records/blob/master/rules/rules_any.md)

Player   |  Time          |  Game Version  |  Platform  |  Date        |  Video Link
:-------:|:--------------:|:--------------:|:----------:|:------------:|:-------------------------------------------:
Daren_K  |  00:02:56.900  |  SLES00718     |  PS2       |  2018-01-06  |  https://www.youtube.com/watch?v=xcjXO4pttCM
